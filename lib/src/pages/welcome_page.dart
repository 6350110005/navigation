import 'package:flutter/material.dart';
import 'package:navigation/src/models/login_model.dart';

class WelcomePage extends StatelessWidget {
  const WelcomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final LoginModel login = ModalRoute.of(context)!.settings.arguments as LoginModel;
    return Scaffold(
      appBar: AppBar(
        title: Text("Welcome ${login.reportName}"),
      ),
      body: Container(
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              login.reportEmail,
              style: TextStyle(fontSize: 25,fontWeight: FontWeight.bold,),
            ),
            Text(
                login.reportName,
                style: TextStyle(fontSize: 25,fontWeight: FontWeight.bold,),
            ),
            ElevatedButton(
                onPressed: (){
                  Navigator.pop(context,"สวัสดีครับ");
                },
                child: Text("Back")),
          ],
        ),
      ),
    );
  }
}


